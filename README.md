# CIUP Events API

**CIUP Events API** is a javascript library to retrieve **CIUP** events. Multiples options are available like the library itself and the [CIUP EVENTS API Plugin](https://gitlab.com/ciup/ciup-events-api-plugin).

## Version

Current version : 1.0.2

### 1.0.2
- New field : partners
- New html field : agenda_usefull_info_date_html, agenda_usefull_info_hours_html, agenda_usefull_info_text_html, agenda_usefull_info_price_html (before we have only text versions for these fields)
- New field : ticketing-link
- upgrade jquery
- update documentation

## Testing

Want to try it before ? Download the [Github zip](https://gitlab.com/ciup/api) from master branch and open index.html

## Installation

### Download

Download the [Github zip](https://gitlab.com/ciup/api) from master branch and go to **/dist/** folder and keep only the latest zip folder.

**Uncompressed** the zip and add it to your project.

### Javascript Library

The library is dependant to **JQuery**. Do not forget to import before the javascript ciup library.

#### Add script in the footer

```javascript
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <link type="text/css" rel="stylesheet" href="/path/to/ciup-events-api.min.css">
    <script type="text/javascript" src="/path/to/ciup-events-api.min.js"></script>
```

### WP Plugin

1.  Install the [CIUP EVENTS API Plugin](https://gitlab.com/ciup/ciup-events-api-plugin)
2.  Activate the **CIUP EVENTS API Plugin** !
3.  Need some help ? Check the [documentation](https://gitlab.com/ciup/api) of the plugin.

## Settings

| Options  | Type    | Default   | Description                                                                                                    | Values                                           |
| -------- | ------- | --------- | -------------------------------------------------------------------------------------------------------------- | ------------------------------------------------ |
| header   | boolean | true      | Show or not the header                                                                                         | true, false                                      |
| footer   | boolean | true      | Show or not the footer ?                                                                                       | true, false                                      |
| layout   | string  | "default" | Choose your layout disposition : If you choose grid-number, the number will be the number of elements per row. | default, grid-2, grid-3, grid-4, grid-5, grid- 6 |
| elements | integer | 4         | Number of elements to show globally.                                                                           | Min value : 1                                    |
| place    | array   | []        | Slug of the place ? You can filter on any place.                                                               | See the list at the end of the **README**        |
| theme    | array   | []        | Slug of the theme ? You can filter on any theme.                                                               | See the list at the end of the **README**        |
| period   | string  | "future"  | Filter on past or future events.                                                                               | "past", "future"                                 |
| page     | integer | 1         | Pagination                                                                                                     | Min value : 1                                    |

## Result

Return values from the new Ciup();

### Return the list of items
| Return | Type |
|--|--|
| id | integer |
| thumbnail | url |
| title | string |
| excerpt | string |
| description | string |
| html_description | html |
| link | url |
| theme | string |
| place | string |
| agenda_start | string |
| agenda_start_datetime | date (ISO-8601) |
| agenda_end | string |
| agenda_end_datetime | date (ISO-8601) |
| modified_date | date |
| agenda_usefull_info_date | string |
| agenda_usefull_info_date_html |html |
| agenda_usefull_info_hours | string |
| agenda_usefull_info_hours_html |html |
| agenda_usefull_info_price | string |
| agenda_usefull_info_price_html |html |
| agenda_usefull_info_text | string |
| agenda_usefull_info_text_html |html |
| partners |object (see [partners list object](#partners-list-object)) |
| ticketing-link | url |

### partners list object
| Return | Type |
|--|--|
| title | string |
| description | string |
| list | array (see [partner object](#partner-object)) |

### partner object
| Return | Type |
|--|--|
| title | string |
| logo | url |
| link | url |

### Return the number of items
| Return | Type |
|--|--|
| posts_counts | integer |

## Usage

```javascript
    $( document ).ready(function() {

         // Retrieve data only and custom html
         var c1 = new Ciup({
             "header" : false,
             "footer" : false,
             "layout" : "default",
             "elements" : 5,
             "place" : "fondation-etats-unis",
             "theme" : "exposition",
             "period" : "future"
             "page" : 1
         }).init();

         c1.then(function(res){
             $('.slider').append('Nombre d\'événement : '+res.posts_counts+'');
             $.each(res.items, function(key, val) {
                 $('.slider').append('<p class="agenda-end">'+val['agenda_end']+'</p>');
                 $('.slider').append('<p class="agenda-start">'+val['agenda_start']+'</p>');
                 $('.slider').append('<p class="description">'+val["description"]+'</p>');
                 $('.slider').append('<p class="id">'+val["id"]+'</p>');
                 $('.slider').append('<p class="link">'+val["link"]+'</p>');
                 $('.slider').append('<p class="place">'+val["place"]+'</p>');
                 $('.slider').append('<p class="theme">'+val["theme"]+'</p>');
                 $('.slider').append('<img class="thumbnail" src="'+val["thumbnail"]+'">');
                 $('.slider').append('<h1 class="title">'+val["title"]+'</h1>');
             });
         });

         // Render the events with the specified layout
         var c3 = new Ciup({
             "header" : true,
             "footer" : true,
             "layout" : "grid-4",
             "elements" : 8,
             "place" : "",
             "theme" : "",
             "period" : ""
         });

         c3.render('.render-default');

     });
```

## List of themes and places

### Themes

|              |                  |            |          |             |                 |
| ------------ | ---------------- | ---------- | -------- | ----------- | --------------- |
| cinema       | conference-debat | exposition | festival | innovation  | insolite        |
| jeune-public | musique          | rencontre  | sport    | theatre     | visites-guidees |
| cirque       | colloque         | danse      | digital  | litterature |

### Places

|                                     |                                          |                                  |                                          |
| ----------------------------------- | ---------------------------------------- | -------------------------------- | ---------------------------------------- |
| bibliotheque                        | college-espagne                          | college-franco-britannique       | college-neerlandais                      |
| comptoir-coreen                     | espace-exposition-plein-air              | fondation-abreu-de-grancher      | fondation-avicenne                       |
| fondation-biermans-lapotre          | fondation-danoise                        | fondation-chine                  | fondation-monaco                         |
| fondation-etats-unis                | fondation-deutsch-de-la-meurthe          | fondation-hellenique             | fondation-lucien-paye                    |
| fondation-suisse                    | fondation-victor-lyon                    | oblique                          | maison-argentine                         |
| maison-ile-de-france                | maison-inde                              | maison-italie                    | maison-coree                             |
| maison-tunisie                      | pavillon-habib-bourguiba                 | maison-norvege                   | maison-eleves-ingenieurs-arts-et-metiers |
| maison-etudiants-armeniens          | maison-etudiants-canadiens               | maison-etudiants-asie-du-sud-est | maison-etudiants-francophonie            |
| maison-etudiants-suedois            | maison-industries-agricoles-alimentaires | maison-provinces-france          | maison-bresil                            |
| maison-cambodge                     | maison-japon                             | maison-liban                     | maison-maroc                             |
| maison-mexique                      | maison-portugal                          | maison-heinrich-heine            | maison-internationale                    |
| maison-internationale-agroparistech | miksi-coworking                          | parc-cite-internationale         | residence-andre-honnorat                 |
| residence-julie-victoire-daubie     | residence-lila                           | residence-robert-garric          | restaurant-universitaire                 |
| salle-polyvalente                   | studios-musique                          | theatre-cite-internationale      | exterieur-parc-andre-citroen             |
| rsi                                 |

## Bugs and feature requests

If you find a bug, please report [it here on Github.](https://gitlab.com/ciup/api/issues/new)

Guidelines for bug reports:

Use the GitHub issue search — check if the issue has already been reported.
Check if the issue has been fixed — try to reproduce it using the latest master branch in the repository.
A good bug report shouldn't leave others needing to chase you up for more information. Please try to be as detailed as possible in your report.

Feature requests are welcome. Please look for existing ones and use GitHub's "reactions" feature to vote.

## License

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
