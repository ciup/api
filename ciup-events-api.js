/**
 * Name:              CIUP Events API
 * Depends:           JQuery
 * Description:       CIUP Events API is a JS file to retrieve events from the CIUP website.
 * Version:           1.0.2
 * Author:            LEAD OFF
 * Author URI:        http://www.leadoff.fr
 * License:           CC BY-NC-SA 4.0
 * License URI:       https://creativecommons.org/licenses/by-nc-sa/4.0/
 * Docs: https://gitlab.com/ciup/api/
 * Repo: https://gitlab.com/ciup/api/
 * Issues: https://gitlab.com/ciup/api/issues
 */

'use strict';

class Ciup {
    // The constructor
    constructor(args) {
        this.ciup_url = "https://www.citescope.fr";

        this.args = args || {
            "header" : false,
            "footer" : false,
            "layout" : "default",
            "elements" : 4,
            "place" : Array,
            "theme" : Array,
            "period" : "",
            "page" : 1
        };
    }

    // Methods

    // Get all the data.
    init() {

        // Prevent XSS injections
        this.args = this.escape(this.args);

        // Init the api url
        let api_url_created = this.ciup_url+"/wp-json/ciup/v2/events?";

        // The number of elements to show
        if(Number.isInteger(parseInt(this.args.elements))) {
            if(Number.isInteger(parseInt(this.args.elements)) > 20){
                this.args.elements = 20;
            }
            api_url_created += "per_page="+this.args.elements+"";
        } else {
            throw ": Elements is not a number, must be a integer";
        }

        if(this.args.page === undefined) {
            this.args.page = 1;
        }

        // The number of page to show
        if(Number.isInteger(parseInt(this.args.page))) {
            api_url_created += "&page="+this.args.page+"";
        } else {
            throw ": Page is not a number, must be a integer";
        }
       

        // The event related to this place
        if(this.args.place.length > 0 && $.type(this.args.place) === "array") {
            let places = "";
            this.args.place.forEach(function(place, index, array){
                places += place;
                if (index !== array.length - 1){ 
                    places += ",";
                }
             });
            api_url_created += "&place="+places;
        } else if($.type(this.args.place) !== "array") {
            throw ": "+this.args.place+" must be an array";
        }

        // The event related to this theme
        if(this.args.theme.length > 0 && $.type(this.args.theme) === "array") {
            let themes = "";
            this.args.theme.forEach(function(theme, index, array){
                themes += theme;
                if (index !== array.length - 1){ 
                    themes += ",";
                }
             });
            api_url_created += "&theme="+themes;
        } else if($.type(this.args.theme) !== "array") {
            throw ": "+this.args.theme+" must be an array";
        }

        // The events results from period
        if(this.args.period == "past" && $.type(this.args.period) === "string") {
            api_url_created += "&period="+this.args.period+"";
        } else if($.type(this.args.period) !== "string") {
            throw ": "+this.args.period+" must be a string";
        }

        return new Promise((resolve, reject) => {
            $.ajax({
                url: api_url_created,
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                success: function(result){
                    // Resolve the promise
                    console.log(result);
                    resolve(result);
                },
                error: function(data){
                    // Return promise error
                    reject(data.responseText);
                }
            });
        });
        
    }

    // Render the data
    render(render_class) {
        this.init().then(res => {

            let result = res.items;

            let render_html;

            // Title in the header
            const header_title = "L'agenda des événements de la Cité Internationale";

            // Render without grid
            if(this.args.layout == "default" || this.args.layout == "") {
                render_html = `<div class="ciup-events-container">`;

                    // Show header or not
                    if(this.args.header == true) {
                        render_html += `<div class="ciup-header">
                            <h2 class="ciup-header-title">${header_title}</h2>
                        </div>`;
                    }

                    render_html += `<div class="ciup-list-events">`;

                        $.each(result, function(key, data) {
                            render_html += `<div class="ciup-event">
                                <a class="ciup-event-link" href="${data.link}" target="_blank">
                                    <div class="ciup-event-head">
                                        <img class="ciup-event-thumbnail" src="${data.thumbnail}" alt="">
                                    </div>
                                    <div class="ciup-event-description">
                                        <p class="ciup-event-dates">${data.agenda_start} au ${data.agenda_end}</p>
                                        <h3 class="ciup-event-title">${data.title}</h3>
                                        <p class="ciup-event-excerpt">${data.excerpt}</p>
                                    </div>
                                </a>
                            </div>`;
                        });

                    render_html += `</div>`;

                    // Show footer or not
                    if(this.args.footer == true) {
                        render_html += `<div class="ciup-footer">
                            <a class="ciup-footer-button" href="${this.ciup_url}" target="_blank">Voir tous les événements</h2>
                        </div>`;
                    }

                render_html += `</div>`;
            }

            // Split the parameter grid-6 and get each part.
            let grid = this.args.layout.split("-");

            // Some control
            if(grid[0] == "grid"){
                // If it's a number then continue
                if(Number.isInteger(parseInt(grid[1]))) {
                    render_html = `<div class="ciup-events-container">`;
                    
                        if(this.args.header == true) {
                            render_html += `<div class="ciup-header">
                                <h2 class="ciup-header-title">${header_title}</h2>
                            </div>`;
                        }

                        render_html += `<div class="ciup-list-events ciup-${this.args.layout}">`;

                            $.each(result, function(key, data) {
                                render_html += `<div class="ciup-event">
                                    <a class="ciup-event-link" href="${data.link}" target="_blank">
                                        <div class="ciup-event-head">
                                            <img class="ciup-event-thumbnail" src="${data.thumbnail}" alt="">
                                        </div>
                                        <div class="ciup-event-description">
                                            <p class="ciup-event-dates">${data.agenda_start} au ${data.agenda_end}</p>
                                            <h3 class="ciup-event-title">${data.title}</h3>
                                            <p class="ciup-event-excerpt">${data.excerpt}</p>
                                        </div>
                                    </a>
                                </div>`;
                            });

                        render_html += `</div>`;

                        if(this.args.footer == true) {
                            render_html += `<div class="ciup-footer">
                                <a class="ciup-footer-button" href="${this.ciup_url}" target="_blank">Voir tous les événements</h2>
                            </div>`;
                        }

                    render_html += `</div>`;
                } else {
                    throw ": The layout grid parameter must be an string with number !";
                }
            }

            // Render in the class.
            $(""+render_class+"").append(render_html);
        });
    }

    // Escape special caracters to prevent XSS attacks
    escape(args) {

        const tagsToReplace = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;'
        };

        $.each(args, function(key, val){
            if($.type(val) === "string") {
                val = val.replace(/[&<>]/g, function(tag) {
                    return tagsToReplace[tag];
                });
                args[key] = val;
            }
        });

        return args;
    };
}
